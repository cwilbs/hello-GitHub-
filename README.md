# hello-GitHub
This is just me getting used to the ins-and-outs of different languages, tools and tricks.
Consider it a quasi-sandbox of plaything for me to experiment with. 
If perusing this area, please keep in mind a few things about me:
* I'm new and still full of mistakes
* I appreciate all constructive criticism
* I make no promises of the security and functionality of anything here, **use at your own risk**



** Log **
----------
18 Jan, 2018
* I've been working through the Webdeveloper's Bootcamp on Udemy and I'll be using this repo to post my notes, exercises and any works-in-progress.
----------
